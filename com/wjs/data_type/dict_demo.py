# Python内置数据类型是有四种：

# ③字典（dict）。Python内置了字典：
# dict的支持，dict全称dictionary，在其他语言中也称为map，使用键-值（key-value）存储，具有极快的查找速度。

d = {'Michael': 95, 'Bob': 75, 'Tracy': 85}
#获取字典中值的方式
a = d.get("Michael")
s = d["Michael"]
if a == s:
    print("获取字典的两种方式")

#判断key是否存在的方式有两种:
#1."Michael" in d
#2.d.get("Michael")
if "Michael" in d:
    print("判断key是否存在的两种方式")
if d.get("不存在的key") == None:
    print("判断key是否存在的两种方式")

# 要删除一个key，用pop(key)方法，对应的value也会从dict中删除：
d.pop("Michael")
print("Michael" in d)

# 请务必注意，dict内部存放的顺序和key放入的顺序是没有关系的。
# 和list比较，dict有以下几个特点：
#
# 查找和插入的速度极快，不会随着key的增加而变慢；
# 需要占用大量的内存，内存浪费多。
# 而list相反：
#
# 查找和插入的时间随着元素的增加而增加；
# 占用空间小，浪费内存很少。
# 所以，dict是用空间来换取时间的一种方法。
