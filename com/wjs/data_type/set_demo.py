# Python内置数据类型是有四种：
#
# ④set（集合）
# set和dict类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在set中，没有重复的key。
#
# 要创建一个set，需要提供一个list作为输入集合：

#重复的自动过滤掉
s = set([1, 2, 3, 4, 1])
print(s)  #结果：{1, 2, 3, 4} python中只有set是{}

#通过add()方式添加元素，remove()删除元素
s.add("wjs")
print(s)
s.remove("wjs")

#set可以看成数学意义上的无序和无重复元素的集合，因此，两个set可以做数学意义上的交集、并集等操作：
s2 = set([1, 4, 5, 6])
print(s | s2)
print(s & s2)

#set和dict的唯一区别仅在于没有存储对应的value，但是，set的原理和dict一样，
# 所以，同样不可以放入可变对象，因为无法判断两个可变对象是否相等，也就无法保证set内部“不会有重复元素”。
#
#set中一般只放入整数和字符串，list是不允许的