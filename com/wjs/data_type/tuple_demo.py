# Python内置数据类型是有四种：

# ②元组：tuple。tuple和list非常类似，但是tuple一旦初始化就不能修改，也可叫有序列表

# 现在，classmates这个tuple不能变了，它也没有append()，insert()这样的方法。
# 其他获取元素的方法和list是一样的，
# 你可以正常地使用classmates[0]，classmates[-1]，但不能赋值成另外的元素。
classmates = ('Michael', 'Bob', 'Tracy')


# 如果要定义一个空的tuple，可以写成()：
t = ()

# 要定义一个只有1个元素的tuple
# 错误代码测试
t = ("a")
print(t) # 结果显示，这就相当于直接给t赋值了，而不是定义一个tuple的类型
t = ("a",) # 而不是t(a), Python在显示只有1个元素的tuple时，也会加一个逗号,，以免你误解成数学计算意义上的括号。
print(t)
print("---------------------------------------")

# 注意一点，tuple是不能变的，但是tuple中加入list，list是可变的那么这是否违背了之前的约定能？
# 答案当然是否定的，tuple不变是指：tuple中元素所指的存储地址不能改变，当list中的元素改变后，tuple指向的还是原来的list地址

