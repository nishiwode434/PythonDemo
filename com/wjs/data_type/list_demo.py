# Python内置数据类型是有四种：

# ①列表：list。list是一种有序的集合，可以随时添加和删除其中的元素

classmates = ['Michael', 'Bob', 'Tracy']
len(classmates) #变量classmates就是一个list。用len()函数可以获得list元素的个数

# 如果要取最后一个元素，除了计算索引位置外，还可以用-1做索引，直接获取最后一个元素，以此类推-2， 取倒数第二个，等等
print(classmates[-2])

# list是一个可变的有序表，所以，可以往list中追加元素到末尾：
classmates.append('wujinsheng')
for i in classmates:
    print(i)
print("-----------------------------------------")
# list里面的元素的数据类型也可以不同，比如
classmates.append(13145)
for i in classmates:
    print(i)
print("-----------------------------------------")
#list元素也可以是另一个list，比如：
s = ['python', 'java', ['asp', 'php'], 'scheme']
for i in s:
    if(isinstance(i, list)): # 判读i是不是list 的实例
        for j in i:
            print(j)
    else:
        print(i)
print("-----------------------------------------")
