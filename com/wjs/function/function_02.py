# 函数的参数
#
# 位置参数
# 默认参数
# 可变参数
# 关键字参数
# 命名关键字参数
# 参数组合
#


# 位置参数----x和n，这两个参数都是位置参数，调用函数时，传入的两个值按照位置顺序依次赋给参数x和n。
def power(x, n):
    s = 1
    while n > 0:
        n = n - 1
        s = s * x
    return s
a = power(1, 2)

print("位置参数:"+ str(a))

print("-----------------------------------------------------------")

# 默认参数----给自定义的函数赋初始值 n=2
def power(x, n=2):
    s = 1
    while n > 0:
        n = n - 1
        s = s * x
    return s

print("默认参数:" + str(pow(1, 2)))

print("------------------------------------------------------------")

#可变参数
#
#对和你想的一样就是参数的个数可以变化， 下面的number，在做函数定义的时候，肯定不知道要传入几个
def calc(numbers):
    sum = 0
    for n in numbers:
        sum = sum + n * n
    return sum
calc([1, 2, 3])
calc((1, 3, 5, 7))
#所以，我们把函数的参数改为可变参数：在参数前面加*
def clac_01(*numbers):
    sum = 0
    for n in numbers:
        sum += n*n
    return sum
clac_01(1, 2)
#如果传入一个已经存在的list或者tuple
a = [1, 2, 3]
print(clac_01(*a)) #就是在前面加一个*，就这么简单
b = (2, 3)
print(clac_01(*b))
print("-----------------------------------------------------------")


#关键字参数
#
# 可变参数允许你传入0个或任意个参数，这些可变参数在函数调用时自动组装为一个tuple。
# 而关键字参数允许你传入0个或任意个含参数名的参数，这些关键字参数在函数内部自动组装为一个dict。
def person(name, age, **jj):
    print('name:', name, 'age:', age, 'other:', jj)

person("武js", 11, sex="boy", address = "山阴县")

# 关键字参数有什么用？它可以扩展函数的功能。
# 比如，在person函数里，我们保证能接收到name和age这两个参数，
# 但是，如果调用者愿意提供更多的参数，我们也能收到。
# 试想你正在做一个用户注册的功能，除了用户名和年龄是必填项外，其他都是可选项，利用关键字参数来定义这个函数就能满足注册的需求。

print("-----------------------------------------------------------")


#命名关键字参数
# 上面关键字参数，我们随便自己命名，个数也不限制，随便加，其实这种肆无忌惮的方式也不是我们想要的
#如果要限制关键字参数的名字，就可以用命名关键字参数，例如，只接收city和job作为关键字参数，中间用*隔开
def person(name, age, *, city, job):
    print(name, age, city, job)

print("-----------------------------------------------------------")


# 参数组合

# 在Python中定义函数，可以用
# 必选参数、默认参数、可变参数、关键字参数和命名关键字参数，这5种参数都可以组合使用。
# 但是请注意，参数定义的顺序必须是：必选参数、默认参数、可变参数、命名关键字参数和关键字参数。
def f2(a, b, c=0, *, d, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'd =', d, 'kw =', kw)


# *args是可变参数，args接收的是一个tuple, 你明明上面传了一个list为什么说这里要必须是tuple，那是应为我的list没有重复元素，understand？
# **kw是关键字参数，kw接收的是一个dict。

