#在Python中，定义一个函数要使用def语句，依次写出函数名、括号、括号中的参数和冒号:，
# 然后，在缩进块中编写函数体，函数的返回值用return语句返回。
import math  #引用外部模块


def my_abs(x):
    if x >= 0:
        return x
    else:
        return -x

#如果没有return语句，函数执行完毕后也会返回结果，只是结果为None。
# return None可以简写为return。
# 在Python交互环境中定义函数时，注意Python会出现...的提示。函数定义结束后需要按两次回车重新回到>>>提示符下：

#空函数
def null_function():
    pass    #pass语句什么都不做，那有什么用？实际上pass可以用来作为占位符，比如现在还没想好怎么写函数的代码，就可以先放一个pass，让代码能运行起来, 可以放在任意函数(方法)中
if 19 >= 18:
    pass

#函数可以返回多个值吗？答案是肯定的。
def move(x, y, step, angle=0):
    nx = x + step * math.cos(angle)
    ny = y - step * math.sin(angle)
    return nx, ny

x, y = move(100, 100, 60, math.pi / 6)
print(x, y)
#Python的函数返回多值其实就是返回一个tuple
r = move(100, 100, 60, math.pi / 6)
print(r)



# 小结
#
# 定义函数时，需要确定函数名和参数个数；
#
# 如果有必要，可以先对参数的数据类型做检查；
#
# 函数体内部可以用return随时返回函数结果；
#
# 函数执行完毕也没有return语句时，自动return None。
#
# 函数可以同时返回多个值，但其实就是一个tuple。