#下面我们学习函数

# Python内置了很多有用的函数，我们可以直接调用。
#
# 要调用一个函数，需要知道函数的名称和参数，比如求绝对值的函数abs，只有一个参数。可以直接从Python的官方网站查看文档：
#
# http://docs.python.org/3/library/functions.html#abs
#
# 也可以在交互式命令行通过help(abs)查看abs函数的帮助信息。

str(222)
abs(-1)
int('123')
bool(1)
max(2, 3, 1, -5)