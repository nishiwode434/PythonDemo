#数据类型其实不重要，因为一个变量可以连续不断的赋值
a = 1 #整型
print("整型 " + str(a)) # str系统内置函数，注意类型装换，这里都要转成字符串

a = 1.4 # 浮点型
print("浮点 " + str(a))

a = 'abc'
a = "abc" #字符串的两种写法
print("字符串 " + a)

a = True #布尔, 布尔值可以用and、or和not运算。
b = False
c = a and b # a or b  或者 not a
print("布尔+ 逻辑运算符 " + str(c))

a = None  #空值是Python里一个特殊的值，用None表示。None不能理解为0，因为0是有意义的，而None是一个特殊的空值