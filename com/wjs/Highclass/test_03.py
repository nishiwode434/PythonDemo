#列表生成式

#列表生成式即List Comprehensions，是Python内置的非常简单却强大的可以用来创建list的生成式。

#举个例子，要生成list [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]可以用list(range(1, 11))：
a = list(range(1, 11))
print(a)

# 但如果要生成[1x1, 2x2, 3x3, ..., 10x10]怎么做？
#我第一次想的是这样子
L = []
for value in a:
    L.append(value*value)
print(L)
#其实，可以更加简单, 这个怎么理解呢？首先我们的概念里有C语言方法的定义方式：
# 返回值 函数名（类型1 param1，类型2 param2，...），那么和下面的对照着看是不是很像，value*value 是 for这个方法的返回值
L1 = [value*value for value in a ]
print(L1)

# for循环后面还可以加上if判断，这样我们就可以筛选出仅偶数的平方：
[x * x for x in range(1, 11) if x % 2 == 0]

# 思考？？？？
[m + n for m in 'ABC' for n in 'XYZ']

#依照上面：我们以此类推，其实就是在【】中写了一个方法，生成了一个list而已，只是这样写也是优缺点的人个认为，代码可读性很差，如果【】中的东西很多

#答案：[m + n for m in 'ABC' for n in 'XYZ']：for循环内嵌for循环，比较m n 中的参数是否符合条件，然后返回m+n，