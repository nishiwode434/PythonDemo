# 迭代
#
#典型的是for

#对字典遍历
d = {'a': 1, 'b': 2, 'c': 3}
#对字典的键遍历
for key in d:
    print(key)
#对字典的值遍历
for value in d.values():
    print(value)
#同时迭代
for key, value in d.items():
    print(str(key) + "-->" + str(value))
print("----------------------------------------------------------")
# 如何判断一个对象是可迭代对象呢？方法是通过collections模块的Iterable类型判断：

#从collections 模块中导入 Iterable，（实际中Iterable可以使函数也可以是一个类）
from collections import Iterable

#isinstance（a, b）这个是判断a 是否是b的实例，返回值是布尔型
isinstance('abc', Iterable) # str是否可迭代

isinstance([1,2,3], Iterable) # list是否可迭代

isinstance(123, Iterable) # 整数是否可迭代

print("----------------------------------------------------------")
# 如果要对list实现类似Java那样的下标循环怎么办？
# Python内置的enumerate函数可以把一个list变成索引-元素对

a = ["w", "j", "s"]
for i, value in enumerate(a):
    print(i, value)

#list 内嵌tuple，遍历
for x, y in [(1, 1), (2, 4), (3, 9)]:
    print(x, y)


# 小结：任何可迭代对象都可以作用于for循环，包括我们自定义的数据类型，只要符合迭代条件，就可以使用for循环。